import React from "react";
import {createRoot} from "react-dom/client";
import ReplogApp from "./RepLog/RepLogApp";

//const el = React.createElement('h2', null, 'Lift History!');
//const el = <h2>Lift stuff! test</h2>
const shouldShowHeart = false;
const root = createRoot(document.getElementById('lift-stuff-app'));
root.render(<ReplogApp withHeart={shouldShowHeart} {...window.REP_LOG_APP_PROPS}/>);