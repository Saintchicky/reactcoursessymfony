import React from "react";
import RepLogList from "./RepLogList";
import PropTypes from "prop-types";
import RepLogCreator from './RepLogCreator';
//import RepLogCreator from './RepLogCreatorControlledComponents';

export default function RepLogs(props) {
    const {
        withHeart, 
        highlightedRowId, 
        onRowClick, 
        repLogs, 
        onAddRepLog, 
        numberOfHearts, 
        onHeartChange, 
        onDeleteRepLog, 
        isLoaded, 
        isSavingNewRepLog,
        successMessage,
        newRepLogValidationErrorMessage,
        itemOptions } = props;

    let heart = '';
    if (withHeart) {
        heart = <span>{'❤️'.repeat(numberOfHearts)}</span>;
    }

    
    const calculateTotalWeightLifted = repLogs => repLogs.reduce(
            //prevValue, currentValue
            // permet de faire la somme, il faut mettre 0 pr init 
            // total correspond à la valeur précédente
            /*
                This creates a variable that is set to a function that accepts one argument: repLogs. 
                Because the function doesn't have curly braces, it means the function returns 
                the result of repLogs.reduce(). The reduce() function - which you may not be familiar 
                with - itself accepts a callback function with two arguments. Once again, 
                because that function doesn't have curly braces, 
                it means that it returns total + log.totalWeightLifted.
            */
            (total, log) => total + log.totalWeightLifted, 0
    );
    // la fonction n'est plus utilisée
    function handleFormSubmit(event) {
        event.preventDefault();
        console.log('I love when a good form submits!');
        console.log(event.target.elements.namedItem('reps').value);
        onNewItemSubmit('Big Fat Cat', event.target.elements.namedItem('reps').value);
    }

    return (
        <div className="col-md-7 js-rep-log-table">
                <h2>Lift Stuff {heart}</h2>
                <input 
                    type="range"
                    value={numberOfHearts}
                    onChange={(e) => {
                        // si on ajoute un + cela permet de casté en int car les inputs sont trjs en string
                        onHeartChange(+e.target.value);
                    }} 
                />
                <hr/>
                {successMessage && (
                <div className="alert alert-success text-center">
                    {successMessage}
                </div>
                )}
                <table className="table table-striped">
                    <thead>
                    <tr>
                        <th>What</th>
                        <th>How many times?</th>
                        <th>Weight</th>
                        <th>&nbsp;</th>
                    </tr>
                    </thead>
                    <RepLogList 
                        highlightedRowId={highlightedRowId}
                        onRowClick={onRowClick}
                        repLogs={repLogs} 
                        onDeleteRepLog={onDeleteRepLog}
                        isLoaded={isLoaded}
                        isSavingNewRepLog={isSavingNewRepLog}
                    />
                    <tfoot>
                        <tr>
                            <td>&nbsp;</td>
                            <th>Total</th>
                            <th>{calculateTotalWeightLifted(repLogs)}</th>
                            <td>&nbsp;</td>
                        </tr>
                    </tfoot>
                </table>
                <div className="row">
                    <div className="col-md-6">
                        <RepLogCreator
                            onAddRepLog={onAddRepLog}
                            validationErrorMessage={newRepLogValidationErrorMessage}
                            itemOptions={itemOptions}
                        />
                    </div>
                </div>
        </div>
    )
}

RepLogs.propTypes = {
    withHeart: PropTypes.bool,
    highlightedRowId: PropTypes.any,
    onRowClick: PropTypes.func.isRequired,
    repLogs: PropTypes.array.isRequired,
    onAddRepLog: PropTypes.func.isRequired,
    numberOfHearts: PropTypes.number.isRequired,
    onHeartChange: PropTypes.func.isRequired,
    onDeleteRepLog: PropTypes.func.isRequired,
    isLoaded: PropTypes.bool.isRequired,
    isSavingNewRepLog: PropTypes.bool.isRequired,
    successMessage: PropTypes.string.isRequired,
    newRepLogValidationErrorMessage: PropTypes.string.isRequired,
};