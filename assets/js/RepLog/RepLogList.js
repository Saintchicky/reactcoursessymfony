
import React  from "react";
import PropTypes from "prop-types";

export default function RepLogList(props) {

    const {highlightedRowId, onRowClick, repLogs, onDeleteRepLog, isLoaded, isSavingNewRepLog } = props;
    if (!isLoaded) {
        return (
            <tbody>
                <tr>
                    <td colSpan="4" className="text-center">Loading...</td>
                </tr>
            </tbody>
        );
    }
    const handleDeleteClick = function(event, repLogId) {
        event.preventDefault();
        onDeleteRepLog(repLogId);
    };
    return(
        <tbody>
        {repLogs.map((repLog) => {
            return (
                    // react doit avoir dans une boucle une clé afin d'identifier la ligne si on doit faire des
                    // traitements dessus
                    <tr key={repLog.id} 
                        className={highlightedRowId === repLog.id ? 'info' : ''}
                        onClick={() => onRowClick(repLog.id)}
                    >
                        <td>{repLog.itemLabel}</td>
                        <td>{repLog.reps}</td>
                        <td>{repLog.totalWeightLifted}</td>
                        <td>
                            <a href="#" onClick={(event) => handleDeleteClick(event, repLog.id) }>
                                <span className="fa fa-trash"></span>
                            </a>
                        </td>
                    </tr>
                );
            })
        }
        {isSavingNewRepLog && (
            <tr>
                <td
                    colSpan="4"
                    className="text-center"
                    style={{
                        opacity: .5
                    }}
                >Lifting to the database ...</td>
            </tr>
        )}
        </tbody>
    );

}
// Avec cette lib cela permet de typer les paramètres attendus 
// ds le contructeur comme en php sinon react s'en fou
// yarn add prop-types --dev
RepLogList.propTypes = {
    highlightedRowId: PropTypes.any,
    onRowClick: PropTypes.func.isRequired,
    onDeleteRepLog: PropTypes.func.isRequired,
    isLoaded: PropTypes.bool.isRequired,
    isSavingNewRepLog: PropTypes.bool.isRequired,
};
