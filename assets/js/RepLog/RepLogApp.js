import React, {Component} from "react";
import RepLogs from "./RepLogs";
import PropTypes from "prop-types";
import { v4 as uuid } from 'uuid';
import { getRepLogs, deleteRepLog, createRepLog } from '../api/rep_log_api';

export default class ReplogApp extends Component {
    constructor(props) {
        super(props);
        // déclarer les variables qu'on va utiliser
        this.state = {
            highlightedRowId: null,
            repLogs: [],
            numberOfHearts: 1,
            isLoaded: false,
            isSavingNewRepLog: false,
            successMessage: '',
            newRepLogValidationErrorMessage: ''
        };
        this.successMessageTimeoutHandle = 0;
        // cela permet de lier le this à ce composant uniquement, s'il est appelé ailleurs
        this.handleRowClick = this.handleRowClick.bind(this);
        this.handleAddRepLog = this.handleAddRepLog.bind(this);
        this.handleHeartChange = this.handleHeartChange.bind(this);
        this.handleDeleteRepLog = this.handleDeleteRepLog.bind(this);
    }
    // Methode de react comme render qui permet 
    // React calls it right after our component is rendered to the DOM. 
    //And this is the best place to make any AJAX requests needed to populate your initial state.
    componentDidMount() {
        getRepLogs()
            .then((data) => {
                this.setState({
                    repLogs: data,
                    isLoaded: true
                })
            });
    }
    // a utiliser après le render du composant pour clean par ex certaines variables
    componentWillUnmount() {
        clearTimeout(this.successMessageTimeoutHandle);
    }
    handleRowClick(repLogId) {
        this.setState({highlightedRowId: repLogId});
    }
    handleAddRepLog(item, reps) {
        const newRep = {
            reps: reps,
            item: item
        };
        this.setState({
            isSavingNewRepLog: true
        });
        const newState = {
            isSavingNewRepLog: false
        };
        createRepLog(newRep)
            .then(repLog => {
                this.setState(prevState => {
                    const newRepLogs = [...prevState.repLogs, repLog];
                    return {
                        // on ne peut utiliser un spread operator dans un objet, pour ça on utilise 
                        // @babel/plugin-proposal-object-rest-spread
                        ...newState,
                        repLogs: newRepLogs,
                        isSavingNewRepLog: false
                    };
                });
                this.setSuccessMessage('Rep Log Saved!');
            })
            .catch(error => {
                error.response.json().then(errorsData => {
                    const errors = errorsData.errors;
                    // comme c'est objet et non un array, il faut mettre un index
                    // Wow! We need to do this because errors isn't an array, it's an object with keys.
                    const firstError = errors[Object.keys(errors)[0]];
                    this.setState({
                        ...newState,
                        newRepLogValidationErrorMessage: firstError
                    });
                })
            })
        ;
    }
    handleHeartChange(heartCount) {
        this.setState({
            numberOfHearts: heartCount
        });
    }
    handleDeleteRepLog(id) {
        this.setState((prevState) => {
            return {
                repLogs: prevState.repLogs.map(repLog => {
                    if (repLog.id !== id) {
                        return repLog;
                    }
                    // comme un array merge php
                    //return Object.assign({}, repLog, {isDeleting: true});
                    return {...repLog, isDeleting: true};
                })
            };
        });
        deleteRepLog(id)
            .then(() => {
                // remove the rep log without mutating state
                // filter returns a new array
                this.setState((prevState) => {
                    return {
                        repLogs: prevState.repLogs.filter(repLog => repLog.id !== id)
                    };
                });
                this.setSuccessMessage('Item was Un-lifted!');
            });
    }
    setSuccessMessage(message) {
        this.setState({
            successMessage: message
        });
        clearTimeout(this.successMessageTimeoutHandle);
        this.successMessageTimeoutHandle = setTimeout(() => {
            this.setState({
                successMessage: ''
            });
            this.successMessageTimeoutHandle = 0;
        }, 3000)
    }
    render(){
        //const {highlightedRowId, repLogs} = this.state;
        //const {withHeart} = this.props;

        return (
            // Spread operator ... qu'on passe en attribut
            // That's it! Every prop and state is now being passed to the child as a prop. 
            // The only things we need to pass by hand are any callbacks we need.
            //  highlightedRowId={highlightedRowId}
            // withHeart={withHeart}
            <RepLogs 
                // itemOptions est ds les props
                {...this.props}
                // on passe toutes les variables grâce à un spread operator
                {...this.state}
                onRowClick={this.handleRowClick}
                onAddRepLog={this.handleAddRepLog}
                onHeartChange={this.handleHeartChange}
                onDeleteRepLog={this.handleDeleteRepLog}
            />
        );
    };
}

ReplogApp.propTypes = {
    withHeart: PropTypes.bool,
    itemOptions: PropTypes.array,
};

// par défaut itemOptions est vide ce qui évite que le react crash lorsqu'il bouclera ds repLogList
ReplogApp.defaultProps = {
    itemOptions: []
};
