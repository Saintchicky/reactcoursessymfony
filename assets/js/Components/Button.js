import React from 'react';
import PropTypes from 'prop-types';

export default function Button(props) {
    //  cela permet de récupérer juste la className des autres varibles qu'on envoie 
    // cela evite qu'elle se fasse overider.
    const { className, ...otherProps } = props;

    return (
        <button
            className={`btn ${className}`}
            {...otherProps}
        >{props.children}</button>
    );
}
Button.propTypes = {
    className: PropTypes.string
};

Button.defaultProps = {
    className: ''
};
