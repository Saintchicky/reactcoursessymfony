function fetchJson(url, options) {
    // on accepte que du json ds les headers pr éviter d'avoir des attaques CSRF
    let headers = {
        'Content-Type': 'application/json',
       // 'X-Csrf-Token': window.CSRF_TOKEN,
    };
    return fetch(url, Object.assign({
        // pour garder des cookies et éviter d'attacher un token ds le header
        credentials: 'same-origin',
        headers: headers,
    }, options))
        .then(checkStatus)
        .then(response => {
            if (response.headers.has('X-CSRF-TOKEN')) {
                window.CSRF_TOKEN = response.headers.get('X-CSRF-TOKEN');
            }

            // decode JSON, but avoid problems with empty responses
            // comme il n'y a pas de retour de donnée quand on utilise delete mettre du vide string
            return response.text()
            .then(text => text ? JSON.parse(text) : '')
        });
}
function checkStatus(response) {
    if (response.status >= 200 && response.status < 400) {
        return response;
    }
    const error = new Error(response.statusText);
    error.response = response;
    throw error
}
/**
 * Returns a promise where the data is the rep log collection
 *
 * @return {Promise<Response>}
 */
export function getRepLogs() {
    return fetchJson('/reps')
        .then(data => data.items);
}
export function deleteRepLog(id) {
    return fetchJson(`/reps/${id}`, {
        method: 'DELETE'
    });
}
export function createRepLog(repLog) {
    return fetchJson('/reps', {
        method: 'POST',
        body: JSON.stringify(repLog),
        headers: {
            'Content-Type': 'application/json'
        }
    });
}